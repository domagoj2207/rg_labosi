#ifdef _WIN32
#include <windows.h>             //bit ce ukljuceno ako se koriste windows
#endif

#define _USE_MATH_DEFINES
//  #include <GL/Gl.h>
//  #include <GL/Glu.h>    nije potrebno ako se koristi glut
#include <GL/glut.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <glm/glm.hpp> //glm::vec3...
#include <iomanip>

using namespace std;

typedef struct {
	double x;
	double y;
	double z;
}kordinateVrha;

typedef struct {
	int x;
	int y;
	int z;
}vrhoviKojiCinePoligon;

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************

GLuint window;
GLuint sub_width = 800, sub_height = 600;
int kut = 0;
int delta = 2;
//GLuint width = 700, height = 700;

//GLOBALNNE varijable
extern int brojacVrhova = 0, brojacPoligona = 0, broj_bspline_ucitanih_vrhova = 0;
extern int broj_segmenata = 0, br_iscrt_tang = 0, bspline_size = 0;
extern double scaleX = 1, scaleY = 1, scaleZ = 1, translX = 1, translY = 1, translZ = 1;
extern double T[4][4] = { {0,0,0,0} ,{0,0,0,0} ,{0,0,0,0} ,{0,0,0,0} }, P[4][4] = { {0,0,0,0} ,{0,0,0,0} ,{0,0,0,0} ,{0,0,0,0} };
std::vector<vrhoviKojiCinePoligon> vrhoviPoligona;
std::vector<kordinateVrha> v_copy;
std::vector<kordinateVrha> v;
std::vector<kordinateVrha> v_B;
std::vector<kordinateVrha> bSpline;
std::vector<kordinateVrha> tangente_za_iscrtavanje;
std::vector<kordinateVrha> sve_tangente; //(pocetna,zarvsna) --uredeni par

//orijentacija vektora je zadana sa s=(0,0,1), e=orijentacija koju zelimo postic
kordinateVrha s = { 0.0,0.0,1.0 }; //po�etna orijentacija vektora = s
kordinateVrha e = { 0.0,0.0,0.0 }; // orijentacija koju zelimo posti� = e
kordinateVrha os = { 0.0,0.0,0.0 }; //odre�uje os oko koje je potrebno rotirati objekt
kordinateVrha srediste = { 0.0,0.0,0.0 };

//****************************************************************
//	Function Prototypes.
//****************************************************************
void init();
void myDisplay();
void mojaFunkcija();
void myReshape(int width, int height);
void iscrtaj();
vector<string> mojSpliter(string s, const char& ch);
void skaliraj_translatira();


//******************************************************************
//	Glavni program.
//******************************************************************
int main(int argc, char ** argv)
{
	//std::ifstream in("aircraft747.obj");
	//std::ifstream in("tree.obj");
	//std::ifstream in("tetrahedron.obj");
	//std::ifstream in("bull.obj");
	std::ifstream in("teddy.obj");

	if (!(in.is_open())) {
		printf_s("Cannot open input file.\n");
		//Sleep(3000);
		return 1;
	}
	int brojac_linija = 0;
	string line;
	while (std::getline(in, line)) {

		brojac_linija += 1;
		string g = line.substr(0, 1);
		if (g == "v") {
			vector<string> rez = mojSpliter(line, ' ');
			kordinateVrha kv;
			string sx, sy, sz;
			sx = rez[1];
			sy = rez[2];
			sz = rez[3];
			kv.x = atof(sx.c_str());
			kv.y = atof(sy.c_str());
			kv.z = atof(sz.c_str());
			srediste.x += kv.x;
			srediste.y += kv.y;
			srediste.z += kv.z;

			v_copy.push_back(kv);
			v.push_back(kv);
			brojacVrhova++;
		}
		//vrhovi poligona krecu od 0 pa do n-1
		else if (g == "f") {
			vector<string> rez = mojSpliter(line, ' ');
			vrhoviKojiCinePoligon fp;
			string sx, sy, sz;
			string delimeter = " ";
			sx = rez[1];
			sy = rez[2];
			sz = rez[3];
			fp.x = atof(sx.c_str()) - 1;
			fp.y = atof(sy.c_str()) - 1;
			fp.z = atof(sz.c_str()) - 1;

			vrhoviPoligona.push_back(fp);
			brojacPoligona++;
		}
	}
	in.close();
	srediste.x = srediste.x / brojacVrhova;
	srediste.y = srediste.y / brojacVrhova;
	srediste.z = srediste.z / brojacVrhova;
	/*dobro ucitavanje
	for (int i = 0; i < brojacVrhova; i++) {
		cout << v[i].x << " " << v[i].y << " " << v[i].z << endl;
	}*/
	ifstream in2("bSpline.txt");
	while (getline(in2, line)) {
		vector<string> rez = mojSpliter(line, ' ');
		kordinateVrha kv;
		string sx, sy, sz;
		sx = rez[0];
		sy = rez[1];
		sz = rez[2];
		kv.x = atof(sx.c_str());
		kv.y = atof(sy.c_str());
		kv.z = atof(sz.c_str());
		v_B.push_back(kv);
		broj_bspline_ucitanih_vrhova++;
	}
	in2.close();
	cout << "Ukupan broj proizvoljnih tocaka koji definiraju putanju:" << broj_bspline_ucitanih_vrhova << endl;
	cout << "-----------------------------------------------" << endl;
	broj_segmenata = broj_bspline_ucitanih_vrhova - 3;
	//provjera ispisa ucitanog niza tocaka koji odreduju aproksim. uniformnu kubnu B-splajn krivulju
	for (int i = 0; i < broj_bspline_ucitanih_vrhova; i++) {
		cout << v_B[i].x << " " << v_B[i].y << " " << v_B[i].z << endl;
	}
	float a, b, c, d, ta, tb, tc, td;
	int br_ulaza = 0; //koliko puta se t povecao
	//za svaki segement krivulje mijenjanje parametra => od 0 do <1
	for (int i = 0; i < broj_segmenata; i++) {
		kordinateVrha kv0, kv1, kv2, kv3;
		kv0 = v_B[i];
		kv1 = v_B[i+1];
		kv2 = v_B[i+2];
		kv3 = v_B[i+3];
		br_ulaza = 0;
		for (float t = 0; t < 1; t += 0.01) {
			//odredivanje tocke krivulje pi(t) -- formula 1.2
			a = (-pow(t, 3) + 3 * pow(t, 2) - 3 * t + 1) / 6;
			b = (3 * pow(t, 3) - 6 * pow(t, 2) + 4) / 6;
			c = (-3 * pow(t, 3) + 3 * pow(t, 2) + 3 * t + 1) / 6;
			d = (pow(t, 3)) / 6;
			kordinateVrha tocka_krivulje;
			tocka_krivulje.x = a * kv0.x + b * kv1.x + c * kv2.x + d * kv3.x;
			tocka_krivulje.y = a * kv0.y + b * kv1.y + c * kv2.y + d * kv3.y;
			tocka_krivulje.z = a * kv0.z + b * kv1.z + c * kv2.z + d * kv3.z;
			//pohrana trenutne tocke krivulje
			bSpline.push_back(tocka_krivulje);
			//pocetna tocka tangente je na krivulji, a zavrsna = pocetna+vektor*koef
			sve_tangente.push_back(tocka_krivulje);
			//smjer tangente - vektor u trenutnoj tocki -- formula 1.4
			ta = (-pow(t, 2) + 2 * t - 1) / 2;
			tb = (3 * pow(t, 2) - 4 * t) / 2;
			tc = (-3 * pow(t, 2) + 2 * t + 1) / 2;
			td = (pow(t, 2)) / 2;
			kordinateVrha vektor_tang;
			vektor_tang.x = ta * kv0.x + tb * kv1.x + tc * kv2.x + td * kv3.x;
			vektor_tang.y = ta * kv0.y + tb * kv1.y + tc * kv2.y + td * kv3.y;
			vektor_tang.z = ta * kv0.z + tb * kv1.z + tc * kv2.z + td * kv3.z;
			kordinateVrha zavrsna_tocka;
			zavrsna_tocka.x = tocka_krivulje.x + vektor_tang.x*0.5; //na 1.5 dosta velike
			zavrsna_tocka.y = tocka_krivulje.y + vektor_tang.y*0.5;
			zavrsna_tocka.z = tocka_krivulje.z + vektor_tang.z*0.5;
			sve_tangente.push_back(zavrsna_tocka);
			//crtat cemo 5 tangenti za jednu krivulju  
			if (br_ulaza == 0 || br_ulaza == 20 || br_ulaza == 40 || br_ulaza == 60 || br_ulaza == 80) {
				tangente_za_iscrtavanje.push_back(tocka_krivulje);
				tangente_za_iscrtavanje.push_back(zavrsna_tocka);
			}
			br_ulaza++;
		}
	}
	bspline_size = bSpline.size();
	br_iscrt_tang = tangente_za_iscrtavanje.size();
	cout << "-----------------------------------------------" << endl;
	cout << "ima ukupno " << sve_tangente.size() << " tangenti" << endl; 
	cout << "iscrtat ce se " << tangente_za_iscrtavanje.size() / 2 << " tangenti" << endl;
	for (int i = 0; i < tangente_za_iscrtavanje.size(); i+=2) {
		std::cout << std::fixed << std::setprecision(2);
		cout << i << endl;
		cout << "Prva tocka tangentne:   " << tangente_za_iscrtavanje[i].x << " " << tangente_za_iscrtavanje[i].y << " " << tangente_za_iscrtavanje[i].z << endl;
		cout << "Zavrsna tocka tangente: " << tangente_za_iscrtavanje[i+1].x << " " << tangente_za_iscrtavanje[i+1].y << " " << tangente_za_iscrtavanje[i+1].z << endl;
	}
	for (int i = 0; i < 10; i++) {

	}
	/*glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(sub_width, sub_height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);
	window = glutCreateWindow("Glut OpenGL labos1");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutIdleFunc(mojaFunkcija);*/

	//init();
	skaliraj_translatira();

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(sub_width, sub_height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("labos RG");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutIdleFunc(mojaFunkcija);

	glutMainLoop();
	return 0;
}

void init() {
	// Bijela boja za pozadinu slike: RGBA = 1,1,1,1 (a=1: opaque, a=0: transparent)
	glClearColor(1.0, 1.0, 1.0, 1.0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(-1.0, 1.0, -1.0, 1.0, 1.0, 8.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_DEPTH_TEST);
}

//*********************************************************************************
//	Promjena velicine prozora.
//*********************************************************************************

void myReshape(int width, int height)
{
	sub_width = width;                      	//promjena sirine prozora
	sub_height = height;						//promjena visine prozora
	glViewport(0, 0, sub_width, sub_height);	//  otvor u prozoru
	glMatrixMode(GL_PROJECTION);                // Select The Projection Matrix
	glLoadIdentity();                           // Reset The Projection Matrix

	// ova metoda nejasna
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	//ja sam imao gluOrtho2D(0,width,0,height);
	glMatrixMode(GL_MODELVIEW);                 // Select The Modelview Matrix
	glLoadIdentity();							//	jedinicna matrica
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);		//	boja pozadine
	glClear(GL_COLOR_BUFFER_BIT);				//	brisanje pozadine
	glPointSize(1.0);							//	postavi velicinu tocke za liniju
	glColor3f(0.0f, 0.0f, 0.0f);				//	postavi boju linije
	/*
	sub_width = width;                      	// zapamti novu sirinu prozora
	sub_height = height;				// zapamti novu visinu prozora
	glViewport(0, 0, sub_width, sub_height);	// otvor u prozoru
	glMatrixMode(GL_PROJECTION);			// matrica projekcije
	glLoadIdentity();				// jedinicna matrica
	*/
}

//*********************************************************************************
//	Osvjezavanje prikaza. (nakon preklapanja prozora) 
//*********************************************************************************
int tt = 0;

void myDisplay()
{
	glLoadIdentity();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glTranslatef(-5.0, -5.0, -75.0);
	//glScalef(scaleX, scaleY, scaleZ);
	glBegin(GL_LINE_STRIP);
	//iscrtavanje krivulje
	for (int i = 0; i < bspline_size; i++) {
		glVertex3f(bSpline[i].x, bSpline[i].y, bSpline[i].z);
	}
	glEnd();
	glBegin(GL_LINES);
	for (int i = 0; i < br_iscrt_tang; i += 2) {
		glVertex3f(tangente_za_iscrtavanje[i].x, tangente_za_iscrtavanje[i].y, tangente_za_iscrtavanje[i].z);
		glVertex3f(tangente_za_iscrtavanje[i + 1].x, tangente_za_iscrtavanje[i + 1].y, tangente_za_iscrtavanje[i + 1].z);
	}
	glEnd();

	glTranslatef(bSpline[tt].x, bSpline[tt].y, bSpline[tt].z);

	e.x = sve_tangente[2 * tt + 1].x - sve_tangente[2 * tt].x;
	e.y = sve_tangente[2 * tt + 1].y - sve_tangente[2 * tt].y;
	e.z = sve_tangente[2 * tt + 1].z - sve_tangente[2 * tt].z;

	os.x = s.y*e.z - e.y*s.z;
	os.y = e.x*s.z - s.x*e.z;
	os.z = s.x*e.y - s.y*e.x;
	
	double module_s = sqrt(pow(s.x, 2) + pow(s.y, 2) + pow(s.z, 2));
	double module_e = sqrt(pow(e.x, 2) + pow(e.y, 2) + pow(e.z, 2));
	double kut = (s.x*e.x + s.y*e.y + s.z*e.z) / (module_s*module_e);
	kut = kut * 180 / M_PI;
	glRotatef(kut, os.x, os.y, os.z);
	glTranslatef(-srediste.x, -srediste.y, -srediste.z);
	for (int i = 0; i < brojacPoligona; i++) {
		int prvi, drugi, treci;	//vrhovi krecu od 0 tj umanjeni su za 1 od onih koji su zadani 
		prvi = vrhoviPoligona[i].x;
		drugi = vrhoviPoligona[i].y;
		treci = vrhoviPoligona[i].z;
		glBegin(GL_LINE_STRIP);
		glVertex3f(v[prvi].x, v[prvi].y, v[prvi].z);
		glVertex3f(v[drugi].x, v[drugi].y, v[drugi].z);
		glVertex3f(v[treci].x, v[treci].y, v[treci].z);
		glEnd();
	}
	glBegin(GL_LINES);
	//x-os
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(srediste.x, srediste.y, srediste.z);
	glVertex3f(srediste.x + 2, srediste.y, srediste.z);
	//y-os
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(srediste.x, srediste.y, srediste.z);
	glVertex3f(srediste.x, srediste.y + 2, srediste.z);
	//z-os
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(srediste.x, srediste.y, srediste.z);
	glVertex3f(srediste.x, srediste.y, srediste.z + 2);
	glColor3f(0.0, 0.0, 0.0);
	glEnd();

	tt++;
	if (tt == bspline_size)
		tt = 0;

	glFlush();
}

vector<string> mojSpliter(string s, const char& ch) {

	vector<string> rez;
	string next;

	for (string::const_iterator it = s.begin(); it != s.end(); it++) {
		if (*it == ch) {
			if (!next.empty()) {
				rez.push_back(next);
				next.clear();
			}
		}
		else {
			next += *it;
		}
	}
	if (!next.empty())
		rez.push_back(next);
	return rez;
}

void skaliraj_translatira() {

	double minx = v[0].x;
	double maxx = v[0].x;
	double miny = v[0].y;
	double maxy = v[0].y;
	double minz = v[0].z;
	double maxz = v[0].z;
	for (int i = 1; i < brojacVrhova; i++) {
		if (v[i].x < minx)
			minx = v[i].x;
		if (v[i].x > maxx)
			maxx = v[i].x;
		if (v[i].y < miny)
			miny = v[i].y;
		if (v[i].y > maxy)
			maxy = v[i].y;
		if (v[i].z < minz)
			minz = v[i].z;
		if (v[i].z > maxz)
			maxz = v[i].z;
	}
	//printf("\n\n%lf\n%lf\n%lf\n%lf\n\n", minx, maxx, miny, maxy);
	translX = minx + 1;
	translY = miny + 1;
	if (maxx - minx > 1)
		scaleX = 1 / (maxx - minx);
	else
		translX = (maxx - minx) / 2;
	if (maxy - miny > 1)
		scaleY = 1 / (maxy - miny);
	else
		translY = (maxy - miny) / 2;
	if (maxz - minz > 1)
		scaleZ = 1 / (maxz - minz);
	else
		translZ = (maxz - minz) / 2;
	//printf("\n\n%lf\n%lf\n%lf\n%lf\n\n", scaleX, scaleY,translX, translY);

}

int vrijeme_trenutno = 0; int vrijeme_prije = 0;

//funkcija za prikazivanje scene --> neaktivna funkcija glutIdleFunc() se poziva nakon obrade svih trenutnih dogadaja
void mojaFunkcija() {
	vrijeme_trenutno = glutGet(GLUT_ELAPSED_TIME); //vraca broj milisekundi od poziva glutInit-a
	//cout << vrijeme_trenutno << " " << endl;
	int interval = vrijeme_trenutno - vrijeme_prije;
	if (interval > 10) {
		myDisplay();
		vrijeme_prije = vrijeme_trenutno;
	}
}

/*
void myDisplay()
{
	glLoadIdentity();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//isto bez obzira na koju god skalu i 1000 i 1/1000
	//glScalef(scaleX, scaleY, 0);
	glTranslatef(-translX, -translY, 0);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < bspline_size; i++) {
		//printf_s("Broj ulazaka %d\n", i);
		if (i == bspline_size - 2)
			cout << i << endl;
		glVertex3f(bSpline[i].x, bSpline[i].y, bSpline[i].z);
	}
	glEnd();
	glBegin(GL_LINES);
	for (int i = 0; i < br_iscrt_tang; i+=2) {
		glVertex3f(tangente_za_iscrtavanje[i].x, tangente_za_iscrtavanje[i].y, tangente_za_iscrtavanje[i].z);
		glVertex3f(tangente_za_iscrtavanje[i+1].x, tangente_za_iscrtavanje[i+1].y, tangente_za_iscrtavanje[i+1].z);
	}
	glEnd();

	glTranslatef(bSpline[tt].x, bSpline[tt].y, bSpline[tt].z);
	//zelimo postic istu orijentaciju kao sto je tangenta u toj tocci
	e.x = sve_tangente[2 * tt + 1].x - sve_tangente[2 * tt].x;
	e.y = sve_tangente[2 * tt + 1].y - sve_tangente[2 * tt].y;
	e.z = sve_tangente[2 * tt + 1].z - sve_tangente[2 * tt].z;
	os.x = s.y*e.z - e.y*s.z;
	os.y = e.x*s.z - s.x*e.z;
	os.z = s.x*e.y - s.y*e.x;
	double module_s = sqrt(pow(s.x, 2) + pow(s.y, 2) + pow(s.z, 2));
	double module_e = sqrt(pow(e.x, 2) + pow(e.y, 2) + pow(e.z, 2));
	double kut = (s.x*e.x + s.y*e.y + s.z*e.z) / (module_s*module_e);
	kut = kut * 180 / M_PI;
	glRotatef(kut, os.x, os.y, os.z);
	glTranslatef(-srediste.x, -srediste.y, -srediste.z);
	for (int i = 0; i < brojacPoligona; i++) {
		int prvi, drugi, treci;	//vrhovi krecu od 0 tj umanjeni su za 1 od onih koji su zadani 
		prvi = vrhoviPoligona[i].x;
		drugi = vrhoviPoligona[i].y;
		treci = vrhoviPoligona[i].z;
		glBegin(GL_LINE_STRIP);
		glVertex2f(v[prvi].x, v[prvi].y);
		glVertex2f(v[drugi].x, v[drugi].y);
		glVertex2f(v[treci].x, v[treci].y);
		glEnd();
	}
	glBegin(GL_LINES)
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(srediste.x, srediste.y, srediste.z);
	glVertex3f(srediste.x + 2, srediste.y, srediste.z);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(srediste.x, srediste.y, srediste.z);
	glVertex3f(srediste.x, srediste.y + 2, srediste.z);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(srediste.x, srediste.y, srediste.z);
	glVertex3f(srediste.x, srediste.y, srediste.z + 2);
	glColor3f(0.0, 0.0, 0.0);
	glEnd();

	tt++;
	if (tt == bspline_size)
		tt = 0;
	glFlush();

	
	
	//glPointSize(1.0);
	//glMatrixMode(GL_MODELVIEW);			// matrica modela
	//gluLookAt(eyex, eyey, eyez, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	//iscrtaj();

	glutSwapBuffers();
}

void mojaFunkcija() {
	vrijeme_trenutno = glutGet(GLUT_ELAPSED_TIME); //vraca broj milisekundi od poziva glutInit-a
	//cout << vrijeme_trenutno << " " << endl;
	int interval = vrijeme_trenutno - vrijeme_prije;
	if (interval > 10) {
		myDisplay();
		vrijeme_prije = vrijeme_trenutno;
	}
}
}*/

